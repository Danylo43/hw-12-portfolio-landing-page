import gulp from "gulp";
const { src, dest, series, parallel } = gulp;

import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import * as sass from "sass";
import gulpSass from "gulp-sass";
import bsc from "browser-sync";
const browserSync = bsc.create();
const sassCompiler = gulpSass(sass);

const jsTaskHandler = () => {
  return src("./src/js/*.js").pipe(dest("./dist/js"));
};

const htmlTaskHandler = () => {
  return src("./src/*.html").pipe(dest("./dist"));
};

const cssTaskHandler = () => {
  return src("./src/scss/main.scss")
    .pipe(sassCompiler().on("error", sassCompiler.logError))
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(dest("./dist/css"))
    .pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
  return src("./src/images/**/*.*")
    .pipe(imagemin())
    .pipe(dest("./dist/images"));
};

const cleanDistTaskHandler = () => {
  return src("./dist", { allowEmpty: true, read: false }).pipe(clean());
};

function watcher() {
  gulp.watch("./src/*.html", htmlTaskHandler).on("all", browserSync.reload);
  gulp
    .watch("./src/scss/**/*.{scss, sass, css}", cssTaskHandler)
    .on("all", browserSync.reload);
  gulp
    .watch("./src/scripts/**/*.js", jsTaskHandler)
    .on("all", browserSync.reload);
  gulp
    .watch("./src/images/**/*.*", imagesTaskHandler)
    .on("all", browserSync.reload);
}

export const js = jsTaskHandler;
export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
export const images = imagesTaskHandler;

export const build = series(
  cleanDistTaskHandler,
  parallel(htmlTaskHandler, cssTaskHandler, imagesTaskHandler)
);

export const dev = series(
  build,
  parallel(browserSyncTaskHandler, watcher, jsTaskHandler)
);

function browserSyncTaskHandler() {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });
}
