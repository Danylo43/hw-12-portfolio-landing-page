window.addEventListener("scroll", function () {
  var header = document.querySelector("header");
  var logo = document.querySelector(".logo");
  var navmenu = document.querySelector(".nav-menu");
  var ulmenu = document.querySelector(".ul-menu");
  var scrollY = window.scrollY;

  if (scrollY > 0) {
    logo.classList.add("scrolled");
    navmenu.classList.add("scrolled");
    ulmenu.classList.add("scrolled");
    header.classList.add("scrolled");
  } else {
    logo.classList.remove("scrolled");
    navmenu.classList.remove("scrolled");
    ulmenu.classList.remove("scrolled");
    header.classList.remove("scrolled");
  }
});
